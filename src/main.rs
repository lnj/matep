mod database;
mod endpoints;
mod oauth;
mod schema;
mod templates;
mod ui;

use std::collections::HashMap;
use std::sync::Arc;
use std::time::SystemTime;

use rocket::tokio::sync::Mutex;
use rocket::{catchers, fs::FileServer, launch, routes, serde::Deserialize};

use crate::templates::TemplateExtensions;

use reqwest::Client;

#[macro_use]
extern crate diesel;

use crate::database::Db;

use crate::endpoints::*;

const MATEPAY_CENTRAL_BANK_USER: &str = "mzb";

pub type CsrfState = Arc<Mutex<HashMap<String, Option<CsrfContext>>>>;

pub fn now_in_unix() -> i64 {
    SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_secs()
        .try_into()
        .expect("Value doesn't fit into i64 anymore. Time to upgrade!")
}

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct OAuthConfig {
    pub client_id: String,
    pub client_secret: String,
    pub gitlab_url: String,
    pub own_base_url: String,
}

pub struct CsrfContext {
    target_url: String,
}

#[launch]
async fn rocket() -> _ {
    pretty_env_logger::init();

    let rocket = rocket::build();
    let figment = rocket.figment();
    let oauth_config: OAuthConfig = figment
        .extract_inner("oauth")
        .expect("Reading oauth config");
    let csrf_state: CsrfState = Arc::new(Mutex::new(HashMap::<String, Option<CsrfContext>>::new()));

    rocket
        .attach(Db::fairing())
        .attach(TemplateExtensions::fairing())
        .manage(oauth::OAuth2::new(&oauth_config).unwrap())
        .manage(Client::new())
        .manage(oauth_config)
        .manage(csrf_state)
        .register("/", catchers![ui::catch_unauthorized])
        .mount("/css", FileServer::from("css"))
        .mount("/img", FileServer::from("img"))
        .mount("/fonts", FileServer::from("fonts"))
        .mount(
            "/",
            routes![
                ui::index,
                ui::home,
                ui::transactions,
                ui::login,
                ui::do_transfer,
                ui::get_items,
                ui::locations,
                ui::add_item,
                ui::do_add_item,
                ui::update_item,
                ui::do_update_item,
                ui::payment_required,
                ui::central_bank_transfer,
                ui::do_central_bank_transfer,
                ui::authorize
            ],
        )
        .mount("/", routes![])
        .mount(
            "/api/v1/",
            routes![
                logout,
                account,
                items,
                transactions,
                item,
                transfer,
                add_item,
                update_item,
                locations,
                central_bank_transfer,
                audit
            ],
        )
}
