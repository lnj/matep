/*
 *  database connection code
 *
 * */

use diesel::{dsl::count, prelude::*};
use rocket_sync_db_pools::database;

use crate::schema::*;
// Items that are stored identically in the database to how they are serialized
use crate::endpoints::{Account, Item, Location};

use crate::now_in_unix;

#[derive(Insertable)]
pub struct Token {
    pub token: String,
    pub username: String,
    pub timestamp: i64,
}

#[derive(Insertable)]
#[diesel(table_name = accounts)]
pub struct InsertableAccount {
    username: String,
    balance: i64,
    display_name: String,
}

#[derive(Queryable)]
pub struct Transaction {
    pub id: i32,
    pub timestamp: i64,
    pub sender: String,
    pub receiver: String,
    pub amount: i64,
}

// Without id, as the id will be decided by the database
#[derive(Insertable)]
#[diesel(table_name = transactions)]
pub struct InsertableTransaction {
    pub timestamp: i64,
    pub sender: String,
    pub receiver: String,
    pub amount: i64,
}

#[derive(Insertable)]
#[diesel(table_name = items)]
pub struct InsertableItem {
    pub name: String,
    pub price: i64,
    pub location: String,
}

#[derive(Insertable)]
pub struct TransactionItem {
    transaction_id: i32,
    item_id: i32,
}

#[database("matepay")]
pub struct Db(diesel::SqliteConnection);

impl Db {
    pub async fn create_account(&self, username: String, display_name: String) -> QueryResult<()> {
        self.run(move |c| {
            diesel::insert_into(accounts::table)
                .values(InsertableAccount {
                    username,
                    balance: 0,
                    display_name,
                })
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn check_account_exists(&self, username: String) -> QueryResult<bool> {
        self.run(move |c| {
            accounts::table
                .filter(accounts::username.eq(username))
                .select(count(accounts::username).gt(0))
                .first::<bool>(c)
        })
        .await
    }

    pub async fn add_account_display_name(
        &self,
        username: String,
        display_name: String,
    ) -> QueryResult<()> {
        self.run(move |c| {
            diesel::update(accounts::table)
                .filter(accounts::username.eq(username))
                .set(accounts::display_name.eq(display_name))
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn create_token(&self, token: Token) -> QueryResult<()> {
        self.run(move |c| diesel::insert_into(tokens::table).values(token).execute(c))
            .await?;
        Ok(())
    }
    pub async fn identify_user(&self, token: String) -> QueryResult<Option<String>> {
        self.run(move |c| {
            tokens::table
                .filter(tokens::token.eq(token))
                .select(tokens::username)
                .first(c)
                .optional()
        })
        .await
    }

    pub async fn delete_tokens(&self, username: String) -> QueryResult<()> {
        self.run(move |c| {
            diesel::delete(tokens::table.filter(tokens::username.eq(username))).execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn get_account(&self, username: String) -> QueryResult<Account> {
        self.run(move |c| {
            accounts::table
                .filter(accounts::username.eq(username))
                .first(c)
        })
        .await
    }

    pub async fn get_accounts(&self) -> QueryResult<Vec<Account>> {
        self.run(move |c| accounts::table.load(c)).await
    }

    pub async fn get_items(&self, location: String) -> QueryResult<Vec<Item>> {
        self.run(move |c| items::table.filter(items::location.eq(location)).load(c))
            .await
    }

    pub async fn get_item(&self, item_id: i32) -> QueryResult<Item> {
        self.run(move |c| items::table.filter(items::id.eq(item_id)).first(c))
            .await
    }

    pub async fn get_transactions_for_user(
        &self,
        username: String,
    ) -> QueryResult<Vec<Transaction>> {
        self.run(move |c| {
            transactions::table
                .filter(
                    transactions::sender
                        .eq(username.clone())
                        .or(transactions::receiver.eq(username)),
                )
                .order_by(transactions::timestamp.desc())
                .load(c)
        })
        .await
    }

    pub async fn get_transactions(&self) -> QueryResult<Vec<Transaction>> {
        self.run(move |c| {
            transactions::table
                .order_by(transactions::timestamp.asc())
                .load(c)
        })
        .await
    }

    pub async fn get_transaction_items(&self, transaction_id: i32) -> QueryResult<Vec<i32>> {
        self.run(move |c| {
            transaction_items::table
                .filter(transaction_items::transaction_id.eq(transaction_id))
                .select(transaction_items::item_id)
                .load(c)
        })
        .await
    }

    pub async fn transfer(
        &self,
        from_account: String,
        to_account: String,
        amount: i64,
        items: Vec<i32>,
    ) -> QueryResult<()> {
        self.run(move |c| {
            // Run the queries to add money to the receivers account and to remove money from the senders account
            // in a transaction, so they are stored in one atomic database operation,
            // to make sure the overall amount of money stays the same
            c.transaction(move |conn| -> QueryResult<()> {
                let updated_rows = diesel::update(accounts::table)
                    .filter(accounts::username.eq(&to_account))
                    .set(accounts::balance.eq(accounts::balance + amount))
                    .execute(conn)?;
                assert_eq!(updated_rows, 1);

                let updated_rows = diesel::update(accounts::table)
                    .filter(accounts::username.eq(&from_account))
                    .set(accounts::balance.eq(accounts::balance - amount))
                    .execute(conn)?;
                assert_eq!(updated_rows, 1);

                // Archive transaction
                let transaction_id = diesel::insert_into(transactions::table)
                    .values(InsertableTransaction {
                        timestamp: now_in_unix(),
                        sender: from_account,
                        receiver: to_account,
                        amount,
                    })
                    .returning(transactions::id)
                    .get_result::<i32>(conn)?;

                for item_id in items {
                    diesel::insert_into(transaction_items::table)
                        .values(TransactionItem {
                            item_id,
                            transaction_id,
                        })
                        .execute(conn)?;
                }

                Ok(())
            })
        })
        .await?;
        Ok(())
    }

    pub async fn add_item(&self, name: String, price: i64, location: String) -> QueryResult<()> {
        self.run(move |c| {
            diesel::insert_into(items::table)
                .values(InsertableItem {
                    name,
                    price,
                    location,
                })
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn update_item_price(
        &self,
        id: i32,
        location: String,
        price: i64,
    ) -> QueryResult<()> {
        self.run(move |c| {
            diesel::update(items::table)
                .filter(items::id.eq(id).and(items::location.eq(location)))
                .set(items::price.eq(price))
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn locations(&self) -> QueryResult<Vec<Location>> {
        let locations = self
            .run(move |c| {
                items::table
                    .select(items::location)
                    .distinct()
                    .load::<String>(c)
            })
            .await?;
        Ok(locations
            .into_iter()
            .map(|name| Location { name })
            .collect())
    }

    pub async fn expire_tokens(&self) -> QueryResult<()> {
        self.run(move |c| {
            let now = now_in_unix();
            // This is a bit ugly, but I can't find an abs function or way to substract timestamp from now in diesel.
            // So the result will always be negative. If it is less than minus a day, the token has been created more than a day ago.
            diesel::delete(tokens::table.filter((tokens::timestamp - now).lt(-24 * 60 * 60)))
                .execute(c)
        })
        .await?;
        Ok(())
    }
}
