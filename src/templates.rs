// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

// Rocket
use rocket::fairing::Fairing;
use rocket_dyn_templates::tera::{self, Value};
use rocket_dyn_templates::{Engines, Template};

// Chrono
use chrono::prelude::DateTime;
use chrono::Utc;

// std
use std::collections::HashMap;
use std::time::Duration;
use std::time::SystemTime;

pub struct TemplateExtensions {}

fn format_euro_cents(cents: i64) -> String {
    let euros = cents / 100;
    let cents = cents % 100;
    let negative = euros.is_negative() || cents.is_negative();
    format!("{}{}.{:02}€", if negative { "-" } else { "" }, euros.abs(), cents.abs())
}

#[test]
fn test_format_euro_cents() {
    assert_eq!(format_euro_cents(9999999), "99999.99€");
    assert_eq!(format_euro_cents(99999999999999999), "999999999999999.99€");
}

#[test]
fn test_negative_value() {
    assert_eq!(format_euro_cents(-2050), "-20.50€");
    assert_eq!(format_euro_cents(-1), "-0.01€");
}

impl TemplateExtensions {
    pub fn filter_format_msecs_since_epoch(
        value: &Value,
        _: &HashMap<String, Value>,
    ) -> tera::Result<Value> {
        match value.as_u64() {
            Some(secs) => {
                let time = SystemTime::UNIX_EPOCH + Duration::from_secs(secs);
                let timestr = DateTime::<Utc>::from(time)
                    .format("%d %b %Y %H:%M %Z")
                    .to_string();
                Ok(Value::String(timestr))
            }
            None => Err(tera::Error::msg(
                "Provided value is not an unsigned integer".to_string(),
            )),
        }
    }

    pub fn filter_format_cents(value: &Value, _: &HashMap<String, Value>) -> tera::Result<Value> {
        match value.as_i64() {
            Some(cents) => Ok(Value::String(format_euro_cents(cents))),
            None => Err(tera::Error::msg(
                "Provided value is not an integer".to_string(),
            )),
        }
    }

    pub fn fairing() -> impl Fairing {
        Template::custom(|engines: &mut Engines| {
            engines.tera.register_filter(
                "format_msecs_since_epoch",
                TemplateExtensions::filter_format_msecs_since_epoch,
            );
            engines
                .tera
                .register_filter("format_cents", TemplateExtensions::filter_format_cents)
        })
    }
}
