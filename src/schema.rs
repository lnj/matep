// @generated automatically by Diesel CLI.

diesel::table! {
    accounts (username) {
        username -> Text,
        balance -> BigInt,
        display_name -> Nullable<Text>,
    }
}

diesel::table! {
    items (id) {
        id -> Integer,
        name -> Text,
        price -> BigInt,
        location -> Text,
    }
}

diesel::table! {
    tokens (token) {
        token -> Text,
        username -> Text,
        timestamp -> BigInt,
    }
}

diesel::table! {
    transaction_items (id) {
        id -> Integer,
        transaction_id -> Integer,
        item_id -> Integer,
    }
}

diesel::table! {
    transactions (id) {
        id -> Integer,
        timestamp -> BigInt,
        sender -> Text,
        receiver -> Text,
        amount -> BigInt,
    }
}

diesel::joinable!(tokens -> accounts (username));
diesel::joinable!(transaction_items -> items (item_id));
diesel::joinable!(transaction_items -> transactions (transaction_id));

diesel::allow_tables_to_appear_in_same_query!(
    accounts,
    items,
    tokens,
    transaction_items,
    transactions,
);
