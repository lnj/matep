/*
 *  contains all endpoints for the MatePay API
 *
 * */

use rocket::{
    form::FromForm, //response,
    get,
    http::{Cookie, Status},
    patch,
    post,
    request::{self, FromRequest, Outcome, Request},
    response::status::Custom,
    serde::{json::Json, Deserialize, Serialize},
    Responder,
};

use diesel::result::Error as DieselError;

use crate::{database::Db, MATEPAY_CENTRAL_BANK_USER};

use std::{collections::HashMap, fmt::Debug};

use log::{error, warn};

// enum

#[derive(Debug)]
pub enum AuthError {
    Unauthorized,
    Server,
    DisallowedUser,
}

// structs

#[derive(Clone)]
pub struct AuthenticatedUser {
    pub username: String,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for AuthenticatedUser {
    type Error = AuthError;

    async fn from_request(req: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        let header = req.headers().get("MATEPAY-TOKEN").next();
        let cookie = req.cookies().get("MATEPAY-TOKEN").map(Cookie::value);
        match header.or(cookie) {
            Some(token) => {
                let db = Db::from_request(req)
                    .await
                    .expect("The database fairing should have been already registered");

                db.expire_tokens()
                    .await
                    .expect("Failed to remove expired tokens");

                match db.identify_user(token.to_string()).await {
                    Ok(username) => match username {
                        Some(username) => Outcome::Success(AuthenticatedUser { username }),
                        None => Outcome::Failure((Status::Unauthorized, AuthError::Unauthorized)),
                    },
                    Err(error) => {
                        error!("Datenbank will nicht {:#?}", error);
                        Outcome::Failure((Status::InternalServerError, AuthError::Server))
                    }
                }
            }
            None => Outcome::Failure((Status::Unauthorized, AuthError::Unauthorized)),
        }
    }
}

pub struct CentralBankUser {
    pub user: AuthenticatedUser,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for CentralBankUser {
    type Error = AuthError;

    async fn from_request(req: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        match AuthenticatedUser::from_request(req).await {
            Outcome::Success(AuthenticatedUser { username })
                if username == MATEPAY_CENTRAL_BANK_USER =>
            {
                Outcome::Success(CentralBankUser {
                    user: AuthenticatedUser { username },
                })
            }
            Outcome::Success(AuthenticatedUser { username }) => {
                warn!(
                    "Invalid user {} tried to access central bank interface",
                    username
                );
                Outcome::Failure((Status::Forbidden, AuthError::DisallowedUser))
            }
            Outcome::Failure(e) => Outcome::Failure(e),
            _ => {
                unreachable!(
                    "The AuthenticatedUser guard either returns Failur or Success, never Forward"
                )
            }
        }
    }
}

#[derive(Deserialize)]
pub struct Authentication {
    pub username: String,
    pub password: String,
}

#[derive(Serialize, Responder)]
#[response(status = 201)]
pub struct Token {
    pub token: String,
}

#[derive(Serialize, Queryable)]
pub struct Account {
    pub username: String,
    pub balance: i64,
    pub display_name: Option<String>,
}

#[derive(Serialize, Queryable)]
pub struct Item {
    pub id: i32,
    pub name: String,
    pub price: i64,
    pub location: String,
}

#[derive(Serialize)]
pub struct Transaction {
    pub timestamp: i64,
    pub sender: String,
    pub receiver: String,
    pub amount: i64,
    pub items: Vec<i32>,
}

#[derive(Deserialize)]
pub struct Transfer {
    pub receiver: String,
    pub bought_items: Vec<i32>,
}

#[derive(Deserialize, FromForm)]
pub struct AddItemRequest {
    // id will be determined by database
    name: String,
    price: i64,
    // location will be determined by user who sends the request
}

#[derive(Deserialize, FromForm)]
pub struct UpdateItemRequest {
    price: i64,
}

#[derive(Serialize, Queryable)]
pub struct Location {
    pub name: String,
}

#[derive(Deserialize, FromForm)]
pub struct CentralBankTransfer {
    to_account: String,
    amount: i64,
}

// routes

pub fn handle_database_error<T: Debug>(error: T) -> Custom<()> {
    error!("A database query failed: {:?}", error);
    Custom(Status::InternalServerError, ())
}

pub type MatePayResult<T> = Result<T, Custom<()>>;

#[post("/logout")]
pub async fn logout(db: Db, user: AuthenticatedUser) -> MatePayResult<()> {
    db.delete_tokens(user.username)
        .await
        .map_err(handle_database_error)
}

#[get("/account")]
pub async fn account(db: Db, user: AuthenticatedUser) -> MatePayResult<Json<Account>> {
    db.get_account(user.username)
        .await
        .map_err(handle_database_error)
        .map(Json)
}

#[get("/items/<location>")]
pub async fn items(db: Db, location: String) -> MatePayResult<Json<Vec<Item>>> {
    db.get_items(location)
        .await
        .map_err(handle_database_error)
        .map(Json)
}

#[get("/item/<item_id>")]
pub async fn item(db: Db, item_id: i32) -> MatePayResult<Json<Item>> {
    db.get_item(item_id)
        .await
        .map_err(handle_database_error)
        .map(Json)
}

#[get("/transactions")]
pub async fn transactions(
    db: Db,
    user: AuthenticatedUser,
) -> MatePayResult<Json<Vec<Transaction>>> {
    let transactions = db
        .get_transactions_for_user(user.username)
        .await
        .map_err(handle_database_error)?;

    let mut out = Vec::new();

    // This needs database optimization
    for transaction in transactions {
        out.push(Transaction {
            timestamp: transaction.timestamp,
            sender: transaction.sender,
            receiver: transaction.receiver,
            amount: transaction.amount,
            items: db
                .get_transaction_items(transaction.id)
                .await
                .map_err(handle_database_error)?,
        })
    }

    Ok(Json(out))
}

#[post("/transfer", data = "<transfer>")]
pub async fn transfer(
    db: Db,
    db1: Db,
    db2: Db,
    user: AuthenticatedUser,
    transfer: Json<Transfer>,
) -> MatePayResult<()> {
    assert_eq!(audit(db, user.clone()).await.unwrap().state, AuditState::Ok);

    let sender = user.username.clone();
    let receiver = transfer.receiver.clone();

    // Check that transaction contains at least one item
    if transfer.bought_items.is_empty() {
        return Err(Custom(Status::UnprocessableEntity, ()));
    }

    // Retrieve all items
    let mut items = Vec::new();
    for item_id in &transfer.bought_items {
        match db1.get_item(*item_id).await {
            Ok(item) => {
                if item.location != transfer.receiver {
                    return Err(Custom(Status::Forbidden, ()));
                }
                if item.price < 0 {
                    error!("Refusing purchase of item {item_id} which has negative price");
                    return Err(Custom(Status::Forbidden, ()));
                }

                items.push(item)
            }
            Err(DieselError::NotFound) => return Err(Custom(Status::UnprocessableEntity, ())),
            Err(e) => return Err(handle_database_error(e)),
        }
    }

    // calculate overall price for the items
    let amount = items.iter().map(|item| item.price).sum();

    let receiver_balance = db1
        .get_account(receiver.clone())
        .await
        .map_err(handle_database_error)?
        .balance;

    if receiver_balance.checked_add(amount).unwrap() < 0 {
        return Err(Custom(Status::PaymentRequired, ()));
    }

    let sender_balance = db1
        .get_account(sender.clone())
        .await
        .map_err(handle_database_error)?
        .balance;

    if amount > sender_balance {
        return Err(Custom(Status::PaymentRequired, ()));
    }

    // transfer the money
    db1.transfer(
        sender.clone(),
        receiver.clone(),
        amount,
        transfer.bought_items.clone(),
    )
    .await
    .map_err(handle_database_error)?;

    assert_eq!(audit(db2, user).await.unwrap().state, AuditState::Ok);

    Ok(())
}

#[post("/items", data = "<item>")]
pub async fn add_item(
    db: Db,
    user: AuthenticatedUser,
    item: Json<AddItemRequest>,
) -> MatePayResult<()> {
    if item.price < 0 {
        return Err(Custom(Status::Forbidden, ()));
    }

    db.add_item(item.name.clone(), item.price, user.username)
        .await
        .map_err(handle_database_error)?;
    Ok(())
}

#[patch("/item/<id>", data = "<item>")]
pub async fn update_item(
    db: Db,
    user: AuthenticatedUser,
    id: i32,
    item: Json<UpdateItemRequest>,
) -> MatePayResult<()> {
    if db
        .get_item(id)
        .await
        .map_err(handle_database_error)?
        .location
        != user.username
    {
        return Err(Custom(Status::Forbidden, ()));
    }

    db.update_item_price(id, user.username.clone(), item.price)
        .await
        .map_err(handle_database_error)?;
    Ok(())
}

#[get("/locations")]
pub async fn locations(db: Db) -> MatePayResult<Json<Vec<Location>>> {
    let locations = db.locations().await.map_err(handle_database_error)?;
    Ok(Json(locations))
}

#[post("/central-bank-transfer", data = "<transfer>")]
pub async fn central_bank_transfer(
    db: Db,
    bank: CentralBankUser,
    transfer: Json<CentralBankTransfer>,
) -> MatePayResult<()> {
    match db.get_account(transfer.to_account.clone()).await {
        Err(DieselError::NotFound) => return Err(Custom(Status::UnprocessableEntity, ())),
        Err(e) => return Err(handle_database_error(e)),
        _ => {}
    }

    db.transfer(
        bank.user.username,
        transfer.to_account.clone(),
        transfer.amount,
        Vec::new(),
    )
    .await
    .map_err(handle_database_error)
}

#[derive(Serialize, Debug, PartialEq, Eq)]
pub enum AuditState {
    Ok,
    Inconsistent,
}

#[derive(Serialize)]
pub struct Audit {
    capitalization: i64,
    state: AuditState,
}

#[get("/audit")]
pub async fn audit(db: Db, _user: AuthenticatedUser) -> MatePayResult<Json<Audit>> {
    // Check that total balance adds up to zero
    let mut total_users_amount = 0;
    let users = db.get_accounts().await.map_err(handle_database_error)?;
    for user in users {
        total_users_amount += user.balance;
    }

    // Total amount should be 0, as the central bank account has all money it has given out as negative balance.

    // Check if balances calculated from transactions add up with what we have stored
    let mut account_balances = HashMap::<String, i64>::new();
    let transactions = db.get_transactions().await.map_err(handle_database_error)?;
    for transaction in transactions {
        // Add money to receiver account
        let balance = account_balances.get(&transaction.receiver).unwrap_or(&0);
        account_balances.insert(transaction.receiver, *balance + transaction.amount);

        // Remove money from sender account
        let balance = account_balances.get(&transaction.sender).unwrap_or(&0);
        account_balances.insert(transaction.sender, *balance - transaction.amount);
    }

    let mut db_consistent = true;
    for (username, balance) in account_balances {
        let db_balance = db
            .get_account(username)
            .await
            .map_err(handle_database_error)?
            .balance;
        db_consistent &= db_balance == balance;
    }

    // Calculate the total amount of money given out
    let capitalization = -db
        .get_account(MATEPAY_CENTRAL_BANK_USER.to_string())
        .await
        .map_err(handle_database_error)?
        .balance;

    Ok(Json(Audit {
        capitalization,
        state: if total_users_amount == 0 && db_consistent {
            AuditState::Ok
        } else {
            AuditState::Inconsistent
        },
    }))
}
