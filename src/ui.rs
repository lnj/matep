use rocket::{
    catch,
    form::Form,
    http::{Cookie, CookieJar, SameSite, Status},
    response::status::Custom,
    response::Redirect,
    serde::json::Json,
    time, uri, Request, State, {get, post},
};

use std::collections::HashMap;

use rocket_dyn_templates::Template;

use reqwest::Client;

use itertools::Itertools;

use rand::{distributions::Alphanumeric, Rng};

use serde::{Deserialize, Serialize};

use log::info;

use crate::{oauth::OAuth2, CsrfContext, CsrfState};
use crate::{
    database::{self, Db},
    endpoints::{
        self, handle_database_error, Account, AddItemRequest, AuthenticatedUser,
        CentralBankTransfer, CentralBankUser, Item, Location, MatePayResult, Transfer,
        UpdateItemRequest,
    },
    now_in_unix, OAuthConfig, MATEPAY_CENTRAL_BANK_USER,
};

#[catch(401)]
pub async fn catch_unauthorized<'r>(request: &Request<'r>) -> Redirect {
    Redirect::to(uri!(login(Some(request.uri().to_string()))))
}

#[get("/payment_required")]
pub fn payment_required() -> Template {
    Template::render("payment_required", HashMap::<String, String>::new())
}

#[get("/")]
pub async fn index() -> Template {
    Template::render("index", HashMap::<String, String>::new())
}

#[get("/home")]
pub async fn home(db: Db, user: AuthenticatedUser) -> MatePayResult<Template> {
    let account = endpoints::account(db, user).await?;

    let is_bank = account.username == MATEPAY_CENTRAL_BANK_USER;

    #[derive(Serialize)]
    struct Context {
        account: Account,
        is_bank: bool,
    }

    Ok(Template::render(
        "home",
        Context {
            account: account.into_inner(),
            is_bank,
        },
    ))
}

#[get("/transactions")]
pub async fn transactions(db: Db, db2: Db, user: AuthenticatedUser) -> MatePayResult<Template> {
    let Json(transactions) = endpoints::transactions(db, user).await?;

    #[derive(Serialize)]
    struct GuiTransaction {
        timestamp: i64,
        sender: String,
        receiver: String,
        amount: i64,
        items: Vec<Item>,
    }

    let mut gui_transactions = Vec::new();
    for transaction in transactions {
        let mut gui_transaction = GuiTransaction {
            timestamp: transaction.timestamp,
            sender: transaction.sender,
            receiver: transaction.receiver,
            amount: transaction.amount,
            items: Vec::new(),
        };
        for item in transaction.items {
            let item = db2.get_item(item).await.map_err(handle_database_error)?;
            gui_transaction.items.push(item);
        }
        gui_transactions.push(gui_transaction);
    }

    #[derive(Serialize)]
    struct Context {
        transactions: Vec<GuiTransaction>,
    }

    Ok(Template::render(
        "transactions",
        Context {
            transactions: gui_transactions,
        },
    ))
}

fn selected_items(form: &HashMap<String, String>) -> Vec<i32> {
    form.iter()
        .filter(|(_, v)| *v == "on")
        .map(|(k, _)| k.replace("item-", ""))
        .flat_map(|s| s.parse())
        .sorted()
        .collect()
}

#[test]
fn test_parse_selected_items() {
    let form = HashMap::from_iter(
        [("item-1", "on"), ("item-2", "off"), ("item-3", "on")]
            .iter()
            .map(|(k, v)| (k.to_string(), v.to_string()))
            .collect::<Vec<_>>(),
    );
    let items = selected_items(&form);
    assert_eq!(items, [1, 3]);
}

#[post("/do-transfer", data = "<transfer>")]
pub async fn do_transfer(
    db: Db,
    db1: Db,
    db2: Db,
    user: AuthenticatedUser,
    transfer: Form<HashMap<String, String>>,
) -> MatePayResult<Redirect> {
    let items = selected_items(&transfer);

    let location = if let Some(location) = transfer.get("location") {
        location.clone()
    } else {
        return Err(Custom(Status::UnprocessableEntity, ()));
    };

    info!(
        "{} bought items {:?} from {}",
        user.username.clone(),
        items,
        location
    );

    let transfer = Transfer {
        receiver: location,
        bought_items: items,
    };

    match endpoints::transfer(db, db1, db2, user, Json(transfer)).await {
        Ok(()) => Ok(Redirect::to(uri!(transactions))),
        Err(Custom(status, _)) => match status {
            s if s == Status::PaymentRequired => Ok(Redirect::to(uri!(payment_required))),
            _ => Err(Custom(status, ())),
        },
    }
}

#[get("/login?<target_url>")]
pub async fn login(
    oauth: &State<OAuth2>,
    csrf_state: &State<CsrfState>,
    target_url: Option<String>,
) -> Redirect {
    let (url, csrf_token) = oauth.get_authorization_url();
    match target_url {
        Some(url) => {
            csrf_state.lock().await.insert(
                csrf_token.secret().to_string(),
                Some(CsrfContext { target_url: url }),
            );
        }
        None => {
            csrf_state
                .lock()
                .await
                .insert(csrf_token.secret().to_string(), None);
        }
    }

    Redirect::to(url.to_string())
}

#[get("/items/<location>")]
pub async fn get_items(
    db: Db,
    location: String,
    user: AuthenticatedUser,
) -> MatePayResult<Template> {
    let own_location = user.username == location;

    let items = endpoints::items(db, location.clone()).await?.into_inner();

    #[derive(Serialize)]
    struct Context {
        items: Vec<Item>,
        location: String,
        own_location: bool,
    }

    Ok(Template::render(
        "items",
        Context {
            items,
            location,
            own_location,
        },
    ))
}

#[get("/locations")]
pub async fn locations(db: Db) -> MatePayResult<Template> {
    let locations = endpoints::locations(db).await?.into_inner();

    #[derive(Serialize)]
    struct Context {
        locations: Vec<Location>,
    }

    Ok(Template::render("locations", Context { locations }))
}

#[get("/add-item")]
pub async fn add_item(user: AuthenticatedUser) -> Template {
    #[derive(Serialize)]
    struct Context {
        username: String,
    }

    Template::render(
        "add_item",
        Context {
            username: user.username,
        },
    )
}

#[post("/do-add-item", data = "<item>")]
pub async fn do_add_item(
    db: Db,
    user: AuthenticatedUser,
    item: Form<AddItemRequest>,
) -> MatePayResult<Redirect> {
    endpoints::add_item(db, user, Json(item.into_inner())).await?;

    Ok(Redirect::to(uri!(home)))
}

#[get("/update-item/<id>")]
pub async fn update_item(db: Db, id: i32, user: AuthenticatedUser) -> MatePayResult<Template> {
    let item = endpoints::item(db, id).await?;

    #[derive(Serialize)]
    struct Context {
        name: String,
        username: String,
        id: i32,
    }

    Ok(Template::render(
        "update_item",
        Context {
            name: item.name.clone(),
            username: user.username.clone(),
            id,
        },
    ))
}

#[post("/do-update-item/<id>", data = "<item>")]
pub async fn do_update_item(
    db: Db,
    user: AuthenticatedUser,
    id: i32,
    item: Form<UpdateItemRequest>,
) -> MatePayResult<Redirect> {
    endpoints::update_item(db, user, id, Json(item.into_inner())).await?;

    Ok(Redirect::to(uri!(home)))
}

#[post("/do-central-bank-transfer", data = "<transfer>")]
pub async fn do_central_bank_transfer(
    db: Db,
    bank: CentralBankUser,
    transfer: Form<CentralBankTransfer>,
) -> MatePayResult<Redirect> {
    endpoints::central_bank_transfer(db, bank, Json(transfer.into_inner()))
        .await
        .map_err(handle_database_error)?;

    Ok(Redirect::to(uri!(transactions)))
}

#[get("/central-bank-transfer")]
pub async fn central_bank_transfer(_user: CentralBankUser) -> MatePayResult<Template> {
    Ok(Template::render(
        "central_bank_transfer",
        HashMap::<String, String>::new(),
    ))
}

#[derive(Serialize)]
pub struct OauthUrl {
    url: String,
}

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
struct GitlabUser {
    name: String,
    username: String,
}

#[get("/oauth/authorize?<code>&<state>")]
pub async fn authorize(
    oauth: &State<OAuth2>,
    http_client: &State<Client>,
    oauth_config: &State<OAuthConfig>,
    csrf_tokens: &State<CsrfState>,
    cookies: &CookieJar<'_>,
    db: Db,
    code: String,
    state: String,
) -> MatePayResult<Redirect> {
    let access_token = oauth.oauth_authorize(code).await.unwrap();

    if !csrf_tokens.lock().await.contains_key(&state) {
        return Ok(Redirect::to(uri!("/")));
    }

    let gitlab_user = http_client
        .get(format!("{}/api/v4/user", oauth_config.gitlab_url))
        .header(
            "Authorization",
            &format!("Bearer {}", access_token.secret()),
        )
        .send()
        .await
        .expect("Fetching user info")
        .json::<GitlabUser>()
        .await
        .expect("Parsing user info");

    let token: String = rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(20)
        .map(char::from)
        .collect();

    let exists = db
        .check_account_exists(gitlab_user.username.clone())
        .await
        .map_err(handle_database_error)?;

    if exists {
        // Update display name
        db.add_account_display_name(gitlab_user.username.clone(), gitlab_user.name.clone())
            .await
            .map_err(handle_database_error)?;
    } else {
        db.create_account(gitlab_user.username.clone(), gitlab_user.name)
            .await
            .map_err(handle_database_error)?;
    }

    db.create_token(database::Token {
        token: token.clone(),
        username: gitlab_user.username.clone(),
        timestamp: now_in_unix(),
    })
    .await
    .map_err(handle_database_error)?;

    let mut cookie = Cookie::new("MATEPAY-TOKEN", token);
    // TODO, why is this needed?
    cookie.set_same_site(Some(SameSite::Lax));
    cookie.set_secure(true);
    cookie.set_http_only(true);
    cookie.set_expires(time::OffsetDateTime::now_utc() + time::Duration::days(1));
    cookies.add(cookie);

    if let Some(Some(context)) = csrf_tokens.lock().await.get(&state) {
        let target_url = context.target_url.clone();
        return Ok(Redirect::to(target_url));
    }

    Ok(Redirect::to(uri!(home)))
}
