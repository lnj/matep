# matep – Matepay server implementation using Rust

## Setup

Install diesel cli:
```
cargo install diesel_cli --features sqlite --no-default-features
```

Run database migrations
```
diesel migration run
```

compile and start the application:
```
cargo run
```

## Test without fiddling with OAuth:
Insert fake tokens into your local test DB to bypass Oauth:
```sql
INSERT INTO accounts VALUES ('joz',0);
INSERT INTO accounts VALUES ('mzb',0);
INSERT INTO tokens(token,username,timestamp) VALUES('Hallo','joz',0)
INSERT INTO tokens(token,username,timestamp) VALUES('Hallöchen','mzb',0)
```
```bash
sqlite3 database.sqlite "UPDATE tokens SET timestamp=$(date +%s)"
```
Do something with the API:
```sh
curl -X POST -H 'MATEPAY-TOKEN: Hallöchen' -d '{"to_account": "joz", "amount": 1000}' http://localhost:8000/api/v1/central-bank-transfer
```
It is also wise to insert the token cookie into your browser.
