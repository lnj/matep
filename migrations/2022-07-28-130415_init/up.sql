CREATE TABLE accounts(
  username TEXT PRIMARY KEY NOT NULL,
  balance BIGINT NOT NULL
);

CREATE TABLE tokens(
  token TEXT PRIMARY KEY NOT NULL, 
  username TEXT NOT NULL, 
  timestamp BIGINT NOT NULL,
  FOREIGN KEY (username) REFERENCES accounts(username)
);

CREATE TABLE items(
  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
  name TEXT NOT NULL,
  price BIGINT NOT NULL,
  location TEXT NOT NULL
);

CREATE TABLE transactions(
  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
  timestamp BIGINT NOT NULL, 
  sender TEXT NOT NULL, 
  receiver TEXT NOT NULL, 
  amount BIGINT NOT NULL,
  FOREIGN KEY (sender)   REFERENCES accounts(username),
  FOREIGN KEY (receiver) REFERENCES accounts(username)
);

CREATE TABLE transaction_items(
  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  transaction_id INTEGER NOT NULL,
  item_id INTEGER NOT NULL,
  FOREIGN KEY (transaction_id) REFERENCES transactions (id),
  FOREIGN KEY (item_id) REFERENCES items (id)
);
