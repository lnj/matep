-- This file should undo anything in `up.sql`
DROP TABLE accounts;
DROP TABLE tokens;
DROP TABLE items;
DROP TABLE transactions;
DROP TABLE transaction_items;
